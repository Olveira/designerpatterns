<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Modality extends Model
{
      protected $guarded = [];
      protected $date = [
          'created_at',
          'updated_at'
      ];
}
