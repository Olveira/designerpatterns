<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Modality extends Model
{
    $guarded = [];
    $date = [
        'created_at',
        'updated_at'
    ];
}
