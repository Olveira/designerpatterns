<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModalityPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modality_plan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('value','10','2');
            $table->unsignedBigInteger('modality_id')->index();
            $table->unsignedBigInteger('plan_id')->index();
            $table->foreign('modality_id')->references('id')->on('modalities');
            $table->foreign('plan_id')->references('id')->on('plans');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modality_plan');
    }
}
