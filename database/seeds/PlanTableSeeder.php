<?php

use Illuminate\Database\Seeder;
use App\Models\Plan;

class PlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $plans = [
          [
              'name' =>  'Choir and Bone',
              'note' => 'Choir more strong',
              'description' => 'Strong Arm'
          ],
         [
             'name' =>  'TheSplint',
             'note' => 'Aluno Top',
             'description' => 'Crazy Life'
         ],
         [
             'name' =>  'Armored Body',
             'note' => 'The best Plan',
             'description' => 'Wow'
         ]
      ];

      foreach($plans as $plan) {
          Plan::firstOrCreate($plan);
      }
    }
}
