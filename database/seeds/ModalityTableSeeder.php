<?php

use Illuminate\Database\Seeder;
use App\Models\Modality;

class ModalityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $modalities = [
          [
            'name' =>  'Crossfit',
            'note' => 'Strong',
            'description' => 'StrongMutiplies',
            'base_value' => mt_rand(100, 200)
          ],
         [
             'name' =>  'Spinning',
             'note' => 'up Body',
             'description' => 'rotations',
             'base_value' => mt_rand(10, 200)
         ],
         [
             'name' =>  'Zumba',
             'note' => 'dance',
             'description' => 'Dance',
             'base_value' => mt_rand(10, 80)
         ]
      ];

      foreach($modalities as $modality) {
          Modality::firstOrCreate($modality);
      }
    }
}
