<?php

use Illuminate\Database\Seeder;
use App\Models\Customer;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customers = [
            [
              'name' =>  'David',
              'gender' => 'M',
              'note' => 'Aluno Novato',
              'cellphone' => '92991531387'
            ],
           [
               'name' =>  'Mauricio',
               'gender' => 'M',
               'note' => 'Aluno Top',
               'cellphone' => '92991531387'
           ],
           [
               'name' =>  'Jaqueline',
               'gender' => 'F',
               'note' => 'Aluna Entrou agora',
               'cellphone' => '92991531387'
           ]
        ];

        foreach($customers as $customer) {
            Customer::firstOrCreate($customer);
        }
    }
}
